package com.example.springpodstawyspring.examples.zadanie;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Transfer {
    private String name;
    private String address;
    private String amount;
    private String account;
}
