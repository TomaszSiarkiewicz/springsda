package com.example.springpodstawyspring.examples.zadanie;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayDeque;
import java.util.Queue;


@Service
@Slf4j
public class TransferService {

    private final Queue<Transfer> transferQueue = new ArrayDeque<>();

    public TransferService() {
        transferQueue.offer(new Transfer("ada", "dasa", "dasda", "dada"));
    }

    public Transfer addTransfer(Transfer transfer) {
        transferQueue.add(transfer);
        return transfer;
    }

    public Transfer peekTransfer() {
        return transferQueue.poll();
    }

    public void performTransfer() {
        if (transferQueue.isEmpty()) {
            log.info("no transfers to perform");
        } else {
            transferQueue.poll();
            log.info("transfer performed");

        }
    }
}
