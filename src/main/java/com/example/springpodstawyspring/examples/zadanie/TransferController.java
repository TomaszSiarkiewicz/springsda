package com.example.springpodstawyspring.examples.zadanie;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("transfer")
public class TransferController {

    private final TransferService transferService;

    @PostMapping("new")
    @ResponseStatus(HttpStatus.CREATED)
    public Transfer addTransfer(@RequestBody Transfer transfer) {
        return transferService.addTransfer(transfer);
    }

    @GetMapping("peek")
    public Transfer peekTransfer() {
        return transferService.peekTransfer();
    }

}
