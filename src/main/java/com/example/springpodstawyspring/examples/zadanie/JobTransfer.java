package com.example.springpodstawyspring.examples.zadanie;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
@Slf4j
public class JobTransfer {

    TransferService transferService;

    @Scheduled(fixedDelay = 30000)
    public void performTransfer() {
        transferService.performTransfer();
    }
}
