package com.example.springpodstawyspring.examples.qualifierExample;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

@Controller
public class RentControler {

    private final RentService rentService;

    public RentControler(@Qualifier("basicRentService") RentService rentService) {
        this.rentService = rentService;
    }

    void rent() {
        this.rentService.rent();
    }
}
