package com.example.springpodstawyspring.examples.animals;

import org.springframework.stereotype.Component;

@Component
public class Dog implements Animal {
    @Override
    public String voice() {
        return "dog voice";
    }
}
