package com.example.springpodstawyspring.examples.animals;

import org.springframework.stereotype.Component;

@Component
public class Cat implements Animal {
    @Override
    public String voice() {
        return "cat voice";
    }
}
