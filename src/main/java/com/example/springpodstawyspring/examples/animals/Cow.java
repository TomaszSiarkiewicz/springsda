package com.example.springpodstawyspring.examples.animals;

import org.springframework.stereotype.Component;

@Component
public class Cow implements Animal {

    @Override
    public String voice() {
        return "cow voice";
    }
}
