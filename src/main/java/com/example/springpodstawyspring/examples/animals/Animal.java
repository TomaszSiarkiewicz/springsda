package com.example.springpodstawyspring.examples.animals;

public interface Animal {
    String voice();
}
