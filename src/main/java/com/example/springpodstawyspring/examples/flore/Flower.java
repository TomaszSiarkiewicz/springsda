package com.example.springpodstawyspring.examples.flore;

import org.springframework.stereotype.Component;

@Component
public class Flower {

    public String getName() {
        return "Flower";
    }
}
