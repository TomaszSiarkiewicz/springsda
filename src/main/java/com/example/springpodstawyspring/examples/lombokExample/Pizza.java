package com.example.springpodstawyspring.examples.lombokExample;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Data
//@Value
public class Pizza {

    private String dough;
    private String sauce;
    private int size;
    private List<String> toppings;
}
