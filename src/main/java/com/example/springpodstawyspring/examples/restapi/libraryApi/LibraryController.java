package com.example.springpodstawyspring.examples.restapi.libraryApi;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RequiredArgsConstructor
@RestController
@RequestMapping("library/book")
@Slf4j
public class LibraryController {

    private final LibraryService libraryService;

    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(IllegalArgumentException.class)
    public void handleWxception(Exception e) {
        log.error(e.getMessage());
    }


    @PostMapping("add")
    public Book add(@RequestBody Book book) {
        return libraryService.add(book);
    }

    @GetMapping("get/{id}")
    public Book getBookById(@PathVariable int id) {
        return libraryService.getById(id);
    }

    @GetMapping("get")
    public List<Book> getByTitle(@RequestParam String title) {
        return libraryService.getByTitle(title);
    }

    @PutMapping("update")
    public Book updateBook(@RequestBody Book book) {
        return libraryService.updateBook(book);
    }

    @DeleteMapping("/{id}")
    public Book deleteBook(@PathVariable int id) {
        return libraryService.deleteBook(id);
    }
}

