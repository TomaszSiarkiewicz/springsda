package com.example.springpodstawyspring.examples.restapi.zad;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {

    @GetMapping("get")
    public User getUser(@RequestParam int id) {
        User user = new User(id, "test");
        System.out.println("Get request: " + user);
        return user;
    }

    @ResponseStatus(HttpStatus.I_AM_A_TEAPOT)
    @PostMapping("create")
    public void createUser(@RequestBody User user) {
        System.out.println("New user: " + user);
    }

    @PutMapping("update")
    public void updateUser(@RequestBody User user) {
        System.out.println("Updated user: " + user);
    }

    @DeleteMapping("delete/{id}")
    public void deleteUser(@PathVariable int id) {
        System.out.println("deleting user... id: " + id);
    }

    @GetMapping("get-all")
    public List<User> getAllUsers() {
        System.out.println("Returning all users");
        return Arrays.asList(
                new User(1, " aaaaa"),
                new User(1, " aaaaa"),
                new User(1, " aaaaa")
        );
    }

}
