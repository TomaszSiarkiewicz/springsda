package com.example.springpodstawyspring.examples.restapi;

import lombok.ToString;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("order")
@ToString
public class OrderController {

    @RequestMapping(value = "get", method = RequestMethod.GET)
    public Order getOrder(@RequestParam int id) {
        return new Order(id, "test", 456.5);
    }

    @GetMapping("get2")
    public Order getOrder2() {
        return new Order(1, "test", 456.5);
    }

    @PostMapping("create")
    public void createOrder(@RequestBody Order order) {
        System.out.println(order);
    }

    @DeleteMapping("delete/{id}")
    public void deleteOrder(@PathVariable int id) {
        System.out.println("Deleting odrer.. id: " + id);
    }
}
