package com.example.springpodstawyspring.examples.restapi.libraryApi;


import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LibraryService {
    private final List<Book> books;
    private int lastId;

    public LibraryService() {
        lastId = 4;
        this.books = new ArrayList<>();
        books.add(new Book(1, "Harry Potter", "JKR", 2010));
        books.add(new Book(2, "Harry Potter 2", "JKR", 2013));
        books.add(new Book(3, "Harry Potter 3", "JKR", 2014));
        books.add(new Book(4, "Harry Potter 4", "JKR", 2012));
    }


    public Book add(Book book) {
        Book newBook = book;
        lastId++;
        newBook.setId(lastId);
        books.add(newBook);
        return newBook;
    }

    public Book getById(int id) {
        return books.stream().
                filter(b -> b.getId() == id)
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException("book not found"));
    }

    public List<Book> getByTitle(String title) {
        return books.stream()
                .filter(b -> b.getTitle().contains(title))
                .collect(Collectors.toList());
    }

    public Book updateBook(Book book) {
        Book bookInDB = books.stream()
                .filter(b -> b.getId() == book.getId())
                .findFirst()
                .orElseThrow(IllegalArgumentException::new);
        bookInDB.setAuthor(book.getAuthor());
        bookInDB.setTitle(book.getTitle());
        bookInDB.setPublicationYear(book.getPublicationYear());

        return bookInDB;
    }

    public Book deleteBook(int id) {
        List<Book> found = new ArrayList<Book>();
        for (Book book : books) {
            if (book.getId() == id) {
                found.add(book);
            }
        }
        books.removeAll(found);
        if (found.size() > 0) {
            return found.get(0);
        } else {
            throw new IllegalArgumentException();
        }
    }
}
