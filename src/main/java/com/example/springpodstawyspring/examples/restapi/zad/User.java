package com.example.springpodstawyspring.examples.restapi.zad;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class User {

    private int id;
    private String address;
}
