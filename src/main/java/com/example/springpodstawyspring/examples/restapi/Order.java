package com.example.springpodstawyspring.examples.restapi;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Order {

    private int id;
    private String address;
    private double price;

}
