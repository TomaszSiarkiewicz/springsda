package com.example.springpodstawyspring.examples.testproperties;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
public class Admin {

    private final String usename;
    private final String password;

    public Admin(@Value("${app.admin.username}") String usename,
                 @Value("${app.admin.password}") String password) {
        this.usename = usename;
        this.password = password;
    }


}
