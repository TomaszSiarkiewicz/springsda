package com.example.springpodstawyspring.examples.film;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class FilmConfiguration {
    @Bean
    public Film matrix() {
        return new Film("Matrix", 120);
    }

    @Bean
    @Scope("prototype")
    public Film troy() {
        return new Film("Troy", 196);
    }

    @Bean("lotr")
    public Film returnOfTheKing() {
        return new Film("Lotr: Return of the king", 255);
    }
}
