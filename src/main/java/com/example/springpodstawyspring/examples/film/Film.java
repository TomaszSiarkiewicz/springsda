package com.example.springpodstawyspring.examples.film;

public class Film {

    private String title;
    private final int duration;

    public Film(String title, int duration) {
        this.title = title;
        this.duration = duration;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
