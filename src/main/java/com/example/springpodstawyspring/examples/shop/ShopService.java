package com.example.springpodstawyspring.examples.shop;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ShopService {

    @Autowired
    private BuyService buyService;

    public String buy(Product product) {
        return this.buyService.buyProduct(product);
    }
}
