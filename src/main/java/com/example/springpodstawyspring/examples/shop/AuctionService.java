package com.example.springpodstawyspring.examples.shop;

import org.springframework.stereotype.Service;

@Service
public class AuctionService {

    private final BuyService buyService;

    public AuctionService(BuyService buyService) {
        this.buyService = buyService;
    }

    public String buy(Product product) {
        return this.buyService.buyProduct(product);
    }
}
