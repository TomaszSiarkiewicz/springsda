package com.example.springpodstawyspring.examples.song;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

@Configuration
public class SongConfiguration {
    @Bean
    public Song stehAuf() {
        return new Song("Till Lindemann", "Steh auf");
    }

    @Bean
    @Scope("prototype")
    public Song knebel() {
        return new Song("Till Lindemann", "Knebel");
    }
}
