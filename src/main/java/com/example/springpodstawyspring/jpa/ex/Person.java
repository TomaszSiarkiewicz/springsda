package com.example.springpodstawyspring.jpa.ex;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Entity
public class Person {



    @Id
    @GeneratedValue
    private Integer id;

    @Column(nullable = false)
    private String firstName;

    @Column(name = "last_name", length = 50, nullable = false)
    private String last_name;

    private Integer age;

    @Column(nullable = false, updatable = false)
    private LocalDateTime creationDate;

    public Person(String firstName, String last_name, Integer age, LocalDateTime creationDate) {
        this.firstName = firstName;
        this.last_name = last_name;
        this.age = age;
        this.creationDate = creationDate;
    }
}
