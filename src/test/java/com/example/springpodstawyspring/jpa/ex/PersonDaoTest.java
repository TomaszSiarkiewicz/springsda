package com.example.springpodstawyspring.jpa.ex;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.time.LocalDateTime;

@SpringBootTest
class PersonDaoTest {
    @Autowired
    PersonDao personDao;

    @Test
    void shouldAddPerson() {
        //given
        Person person = new Person("Michal", "Chmielewski", null, LocalDateTime.now());
        //then
        this.personDao.save(person);
    }

}