package com.example.springpodstawyspring;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

@SpringBootTest
class SpringPodstawySpringApplicationTests {

    @Test
    void contextLoads() {
    }
    @Test
    void printBeans(){
        AnnotationConfigApplicationContext cotext = new AnnotationConfigApplicationContext(SpringPodstawySpringApplication.class);

        System.out.println("BEANS: ");

        for (String beanDefinitionName : cotext.getBeanDefinitionNames()){
            System.out.println(beanDefinitionName);
        }
        System.out.println("END BEANS");
    }

}
