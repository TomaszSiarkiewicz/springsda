package com.example.springpodstawyspring.examples.lombokExample;

import org.junit.jupiter.api.Test;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class PizzaTest {

    @Test
    void allArgsConstructorTest(){
        Pizza pizza = new Pizza("standard", "tomato",32, Arrays.asList("bacon", "mozzarella", "ham", "tomato"));

        assertThat(pizza).isNotNull();
    }
    @Test
    void getterTest(){
        Pizza pizza = new Pizza("standard", "tomato",32, Arrays.asList("bacon", "mozzarella", "ham", "tomato"));

        assertThat(pizza).isNotNull();
        assertThat(pizza.getDough()).isNotNull();
    }
    @Test
    void NoArgsConstructorTest(){
        Pizza pizza = new Pizza();

        assertThat(pizza).isNotNull();
    }

}