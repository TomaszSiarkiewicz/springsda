package com.example.springpodstawyspring.examples.song;

import com.example.springpodstawyspring.SpringPodstawySpringApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
class SongTest {

    @Test
    void printBeans() {
        AnnotationConfigApplicationContext cotext = new AnnotationConfigApplicationContext(SpringPodstawySpringApplication.class);

        System.out.println("BEANS: ");

        for (String beanDefinitionName : cotext.getBeanDefinitionNames()) {
            System.out.println(beanDefinitionName);
        }
        System.out.println("END BEANS");
    }

    @Test
    void filmBeanPrototypeTest() {
//        given
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringPodstawySpringApplication.class);

//        when
        Song song = (Song) context.getBean("knebel");
        song.setTitle("Frau & Mann");
        song = (Song) context.getBean("knebel");
//        then
        assertThat(song.getTitle().equals("knebel"));
    }

    @Test
    void filmBeanSingletonTest() {
//        given
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringPodstawySpringApplication.class);

//        when
        Song song = (Song) context.getBean("stehAuf");
        song.setTitle("Frau & Mann");
        song = (Song) context.getBean("stehAuf");
//        then
        assertThat(song.getTitle().equals("Frau & Mann"));
    }
}