package com.example.springpodstawyspring.examples.film;

import com.example.springpodstawyspring.SpringPodstawySpringApplication;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;


@SpringBootTest
class FilmTest {

    @Test
    void filmBeanTest() {
        //given
        AnnotationConfigApplicationContext context =new AnnotationConfigApplicationContext(SpringPodstawySpringApplication.class);

        //when
        Film film = (Film) context.getBean("matrix");

        //then
        assertThat(film.getTitle()).isEqualTo("Matrix");
    }
    @Test
    void filmPrototypeFilmTest() {
        //given
        AnnotationConfigApplicationContext context =new AnnotationConfigApplicationContext(SpringPodstawySpringApplication.class);

        //when
        Film film = (Film) context.getBean("troy");
        film.setTitle("test");
        film = (Film) context.getBean("troy");

        //then
        assertThat(film.getTitle()).isEqualTo("Troy");
    }
}