package com.example.springpodstawyspring.examples.animals;

import com.example.springpodstawyspring.SpringPodstawySpringApplication;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class AnimalTestSuite {

    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(SpringPodstawySpringApplication.class);


    @Test
    void cowVoiceTest(){

        Cow cow = context.getBean(Cow.class);

        assertThat(cow.voice().equals("cow voice"));
    }
    @Test
    void catVoiceTest(){

        Cat cat = context.getBean(Cat.class);

        assertThat(cat.voice().equals("cat voice"));
    }
    @Test
    void dogVoiceTest(){

        Dog dog = context.getBean(Dog.class);

        assertThat(dog.voice().equals("dog voice"));
    }

}