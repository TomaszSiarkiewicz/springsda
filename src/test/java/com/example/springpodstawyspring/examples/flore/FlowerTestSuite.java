package com.example.springpodstawyspring.examples.flore;

import com.example.springpodstawyspring.SpringPodstawySpringApplication;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

class FlowerTestSuite {

    @Test
    void flowerBeanTest() {
        //given
        AnnotationConfigApplicationContext cotext = new AnnotationConfigApplicationContext(SpringPodstawySpringApplication.class);

        //when
        Flower flower = cotext.getBean(Flower.class);

        //then
        System.out.println(assertThat(flower.getName()).isEqualTo("Flower"));
    }

}